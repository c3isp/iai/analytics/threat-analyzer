import time
import pandas
from stix2 import  AttackPattern, CourseOfAction

from datetime import datetime
from stix2 import CustomObservable, properties, ObservedData, parse, Vulnerability, CustomObject, \
    Relationship, Bundle, MemoryStore

from Drill.DrillManager import connect, get_threat_info_from_name
from Drill.DrillSettings import drill_host, drill_port, drill_username, drill_password

ts1 = time.time()

operation_time = datetime.utcnow()


@CustomObject('observing-data', [
                  ('observation_date', properties.StringProperty(required=True)),
                  ('organization_name', properties.StringProperty(required=True)),

              ])
class Observing_object(object):
    pass



@CustomObject('observed-data', [
    ('cpe_key_2', properties.StringProperty(required=True)),
    ('name', properties.StringProperty(required=True)),
    ('description', properties.StringProperty(required=True)),
    ('modified', properties.StringProperty(required=True)),
    ('created', properties.StringProperty(required=True)),

])
class CPE_object(object):
    pass


def create_cve_object(date_published, cvss_value, cwe_key, cve_id, description):
    vulnerability = Vulnerability(
        published=date_published,
        cvss_value=cvss_value,
        cwe_key=cwe_key,
        name=cve_id,
        description=description,
        allow_custom=True
    )
    return vulnerability

def create_ap_object(created_by_ref, created, modified, name, description, external_references):
    attack_pattern = AttackPattern(
        created_by_ref=created_by_ref,
        created=created,
        modified=modified,
        name=name,
        description=description,
        allow_custom=True
    )
    return attack_pattern



def create_course_of_action(created_by_ref, created, modified, name, description):
    course_of_action= CourseOfAction(
        created_by_ref=created_by_ref,
        created=created,
        modified=modified,
        name=name,
        description=description,
        allow_custom=True
    )
    return course_of_action

def create_observing_object(organization_name):
    stix_observing_object = Observing_object(
        observation_date=str(operation_time.isoformat()),
        organization_name=organization_name,
        allow_custom=True
    )
    return stix_observing_object

def create_cpe_object(cpe_key_2, description, modified, name, created):
    stix_cpe_object = CPE_object(
        cpe_key_2=cpe_key_2,
        name=name,
        description=description,
        modified=modified,
        created=created
    )
    return stix_cpe_object

def get_stix_from_name(name_app, organization_name):
    print("Drill Connection....")
    drill = connect(drill_host, drill_port, drill_username, drill_password)
    print("Drill Connected....")

    threat_info = get_threat_info_from_name(drill, name_app)


    cve_name_list = []
    cve_list = []
    cpe_list = []
    cpe_name_list = []
    mit_list = []
    mit_name_list = []
    stix_object = ""

    row_query = 0
    len_result = len(threat_info.to_dataframe()) -1

    if len_result == 0:
        print("No results")
        stix_object = "{}"
        return stix_object

    bundle = '{"type": "bundle","id": "bundle--65f82266-4764-4efe-8030-1b568f6cc9cc", "spec_version": "2.0","objects":['

    observing_object = create_observing_object(organization_name)
    stix_object += bundle + str(observing_object) + ","


    for threat in threat_info:

        try:
            index_cpe = cpe_name_list.index(threat["cpe_key_2"])
        except:
            cpe = create_cpe_object(threat["cpe_key_2"], threat["cpe_description"], threat["cpe_modified"],
                                    threat["cpe_name"], threat["cpe_created"])
            cpe_list.append(cpe)
            cpe_name_list.append(threat["cpe_key_2"])
            stix_object += str(cpe) + ","
            index_cpe = len(cpe_list)-1
            relationship = Relationship(observing_object, 'observes', cpe)
            stix_object += str(relationship) + ","


        vuln = create_cve_object(threat["cve_published"], threat["cve_cvss"], threat["cve_cwe"], threat["cve_name"],
                                 threat["cve_summary"])
        try:
            index_cve = cve_name_list.index(threat["cve_name"])
        except:
            cve_list.append(vuln)
            cve_name_list.append(threat["cve_name"])
            stix_object += str(vuln) + ","
            index_cve = len(cve_list)-1

        attack = create_ap_object(threat["ap_created_by_ref"], threat["ap_created"], threat["ap_modified"], threat["ap_name"], threat["ap_description"], '{"source_name":"capec","url":"https://capec.mitre.org/data/definitions/47.html","external_id":"CAPEC-47"}')
        stix_object += str(attack) + ","
        relationship = Relationship(attack, 'targets', cve_list[index_cve])
        stix_object += str(relationship) + ","

        relationship = Relationship(cpe_list[index_cpe], 'has', cve_list[index_cve] )
        stix_object += str(relationship) + ","

        mitigation = create_course_of_action(threat["mit_created_by_ref"], threat["mit_created"], threat["mit_modified"], threat["mit_name"], threat["mit_description"])
        try:
            index_mit = mit_name_list.index(threat["mit_name"])
        except:
            mit_list.append(mitigation)
            mit_name_list.append(threat["mit_name"])
            stix_object += str(mitigation) + ","
            index_mit = len(mit_list) - 1

        relationship = Relationship(mitigation, 'mitigates', attack)
        if row_query == len_result:
            stix_object += str(relationship) + "]}"
        else:
            stix_object += str(relationship) + ","
        row_query +=1
    return stix_object





