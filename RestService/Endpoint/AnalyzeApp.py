from flask import Response
from flask_restplus import Resource
import glob

from Analyzer.Analyze import get_stix_from_name
from Restplus import api

ns_analyze = api.namespace('ThreatAnalyzer', description='Threat analysis of application list')
upload_parser = api.parser()
#upload_parser.add_argument("applications", location='files', type=FileStorage)
upload_parser.add_argument("applications", location='form', type=str)





@ns_analyze.route("/Analyze", methods=['POST'])
class Analyze(Resource):
    '''
    def getStix(self, application):
        content = ""
        stix_saved = glob.glob(STIX_SAVED + "*.json")
        print(stix_saved)
        print(STIX_SAVED + application + ".json")
        if STIX_SAVED + application + ".json" in stix_saved:
            file = open(STIX_SAVED + application + ".json", "r")
            return file.read()
    '''

    #Rest Api used to upload a file
    @api.expect(upload_parser)
    def post(self):
        args = upload_parser.parse_args()
        try:
            application_name = args["applications"]  # This is FileStorage instance
            #application_list = readFile(uploaded_file)
            print(application_name)
            stix_object = get_stix_from_name(application_name, "IIT-CNR")
            print(stix_object)
            #application_list = [uploaded_file]
            #json_stix =self.getStix(uploaded_file)
            #json_stix = analyzeApps(application_list)
        except:
            return Response("Error in uploading file", status=400, mimetype='application/json')



        return Response(stix_object, status=200, mimetype='application/json')

















