import pathlib as pathlib


def checkextension(pathfile):
    extensionPermitted = [".txt"]
    extension = pathlib.Path(pathfile).suffix
    if extension in extensionPermitted:
        return True
    else:
        return False

def readFile(file):
    application_list = []
    if(checkextension(file.filename) == False):
        return None
    else:
        content = file.read().decode('utf-8')
        for appName in content.split("\n"):
            if(appName != ""):
                print(appName)
                application_list.append(appName)
    return application_list