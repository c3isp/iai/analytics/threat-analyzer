import sys

from Settings import CPE_URL, CPE_XML_FILENAME, CPE_ROOT, CPE_XML_PATH, CTI_URL, CTI_ROOT
import requests, zipfile, StringIO, os
from stix2 import CustomObject, properties


def download_cpe_file():
    # ============================================================================================================================================================================================
    print('updating the CPE-DB...')
    r = requests.get(CPE_URL, stream=True)
    cpe_zip_file = zipfile.ZipFile(StringIO.StringIO(r.content))
    # ============================================================================================================================================================================================
    # exctract xml file from zip
    #file_to_operate = os.path.join(CPE_ROOT, CPE_XML_FILENAME)

    cpe_xml = cpe_zip_file.extract(CPE_XML_FILENAME, CPE_ROOT)
    # ============================================================================================================================================================================================
    # get cpe-id with 2.2 version and title of the corresponding application
    print('the CPE-DB was updated and saved to ' + CPE_XML_PATH)



def convert_cpe_to_json():
    print("CPE to JSON conversion...")
    file_to_operate = os.path.join(CPE_ROOT, CPE_XML_FILENAME)
    xml_file = open(file_to_operate).read().splitlines()
    with open(CPE_ROOT + 'stix-cpe-database-with-cve.json', 'w') as datafile:
        datafile.write('[')
        datafile.close()

    total = 0
    for index, line in enumerate(xml_file):
        if '<cpe-item name="' in line:
                total +=1

    cpe_idx = 0

    for index, line in enumerate(xml_file):
        if '<cpe-item name="' in line:
            sys.stdout.write("\r" + str(cpe_idx) + " / " + str(total))
            sys.stdout.flush()
            cpe_idx +=1
            cpe_item_raw = xml_file[index + 0: index + 2]
            cpe_id_22 = str(str(cpe_item_raw)[
                            str(cpe_item_raw).find('<cpe-item name="') + len('<cpe-item name="'):str(cpe_item_raw).rfind(
                                '>\n')]).split('">', 1)[0]
            cpe_name = str(str(cpe_item_raw)[
                           str(cpe_item_raw).find('<title xml:lang="en-US">') + len('<title xml:lang="en-US">'):str(
                               cpe_item_raw).rfind('</title>')]).split('">', 1)[0]



            # ============================================================================================================================================================================================
            # create stix "cpe-platform" custom object for each cpe-id
            # define properties according to the STIX 2.0 specification
            @CustomObject('cpe-platform', [
                ('cpe_key_2', properties.StringProperty(required=True)),
                ('name', properties.StringProperty(required=True)),
                ('description', properties.StringProperty(required=True)),
            ])
            class CPE_object(object):
                pass


            # define properties values from the cpe-xml file
            stix_cpe_object = CPE_object(
                cpe_key_2=cpe_id_22,
                name=cpe_name,
                description='The cpe_key for \"' + cpe_name + '\" application'
            )
            # specify the output
            if cpe_idx == total:
                data = str(stix_cpe_object)
            else:
                data = str(stix_cpe_object) + ','

            # ============================================================================================================================================================================================
            # create json file with all stix "cpe-platform" custom objects
            with open(CPE_ROOT + 'stix-cpe-database-with-cve.json', 'a') as datafile:
                datafile.write(data + '\n')
                datafile.close()
    with open(CPE_ROOT + 'stix-cpe-database-with-cve.json', 'a') as datafile:
        datafile.write(']')
        datafile.close()

    print('xml-data was converted to  ' + os.path.join(os.getcwd() + '\\' + 'stix-cpe-database.json'))

def download_cti():
    zip_file_name = 'master.zip'
    foldername = 'cti-master'
    # ============================================================================================================================================================================================
    print('updating the Att&ck DB...')

    r = requests.get(CTI_URL, stream=True)
    attack_zip_file = zipfile.ZipFile(StringIO.StringIO(r.content))
    for file in attack_zip_file.namelist():
        if file.startswith("cti-master/"):
            attack_db = attack_zip_file.extract(file, CTI_ROOT)
    print('The Att&ck DB was updated')

    return attack_db

download_cpe_file()
convert_cpe_to_json()

download_cti()