CPE_URL = "https://nvd.nist.gov/feeds/xml/cpe/dictionary/official-cpe-dictionary_v2.2.xml.zip"
CPE_ROOT = "/home/giacomo/CNR/Osservatorio_cyber/Threat_identification/CPE_DB/"
CPE_XML_FILENAME = "official-cpe-dictionary_v2.2.xml"
CPE_XML_PATH = CPE_ROOT + CPE_XML_FILENAME


CTI_URL = 'https://github.com/mitre/cti/archive/master.zip'
CTI_ROOT = "/home/giacomo/CNR/Osservatorio_cyber/Threat_identification/CPE_DB/"
CTI_FILENAME = "cti-master"
