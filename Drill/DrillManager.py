
from pydrill.client import PyDrill
from pydrill.exceptions import ImproperlyConfigured

from Drill.DrillSettings import STIX_CPE_PATH, ATTACK_PATTERN_PATH, CAPEC_RELATIONSHIP_PATH, \
    COURSE_OF_ACTION_PATH, CVE_PATH


def connect(host, port, username, password):
    drill = PyDrill(host= host, port= port,  auth=username + ":" + password)

    if not drill.is_active():
        raise ImproperlyConfigured('Please run Drill first')
        return None
    return drill

def get_threat_info_from_name(drill, app_name):

    set_query = drill.query("ALTER SESSION SET `store.json.all_text_mode` = true")

    query_cve_ap_mit = drill.query("SELECT * FROM (SELECT * FROM(SELECT flatten(vulnerable_configuration_cpe_2_2) as cve_cpes,  Modified as cve_modified, cvss as cve_cvss, id as cve_name, summary as cve_summary, Published as cve_published, cwe as cve_cwe "
                                   "FROM dfs.`" + CVE_PATH + "`) as table1  "
                                   "JOIN (SELECT cpe_key_2, description as cpe_description, modified as cpe_modified, name as cpe_name, created as cpe_created  "
                                   "FROM dfs.`" + STIX_CPE_PATH + "` WHERE name = '" + app_name + "') as table2 on table1.cve_cpes = table2.cpe_key_2) as table_1_2 "
                                   " JOIN (SELECT * FROM (SELECT  obj[2].`value` as ap_cwe, *  FROM (SELECT kvgen(flatten(ap.objects.external_references))as obj, ap.objects.id as ap_id, ap.objects.created_by_ref as ap_created_by_ref, "
                                   " ap.objects.created as ap_created, ap.objects.modified as ap_modified, ap.objects.name as ap_name, ap.objects.description as ap_description, ap.objects.external_references as ap_external_references,"
                                   " ap.objects.x_capec_abstraction as ap_x_capec_abstraction, ap.objects.x_capec_example_istances as ap_x_capec_example_istances, ap.objects.x_capec_prerequisites as ap_x_capec_prerequisites,"
                                   " ap.objects.x_capec_resources_required as ap_x_capec_resources_required, ap.objects.x_capec_status as ap_x_capec_status, ap.objects.x_capec_typical_severity as ap_x_capec_typical_severity "
                                   " FROM dfs.`" + ATTACK_PATTERN_PATH + "` as ap))) as table3 on table3.ap_cwe = table_1_2.cve_cwe  JOIN"
                                   " (SELECT rel.objects.source_ref as rel_source_ref, rel.objects.target_ref as rel_target_ref FROM dfs.`" + CAPEC_RELATIONSHIP_PATH + "` rel WHERE rel.objects.relationship_type = 'mitigates') as table4"
                                   " on table3.ap_id = table4.rel_target_ref JOIN (SELECT mit.objects.created_by_ref as mit_created_by_ref, mit.objects.id as mit_id, mit.objects.created as mit_created, mit.objects.name as mit_name, mit.objects.description as mit_description, mit.objects.modified as mit_modified FROM dfs.`" + COURSE_OF_ACTION_PATH + "` mit) as table5"
                                   " on table4.rel_source_ref = table5.mit_id")


    return query_cve_ap_mit