drill_host = "localhost"
drill_username = ""
drill_password = ""
drill_port = 8047
CVE_PATH = "/home/giacomo/CNR/Osservatorio_cyber/Threat_identification/CPE_DB/cve_valid.json"
EDB_PATH = "/home/giacomo/CNR/Osservatorio_cyber/Threat_identification/CPE_DB/cve_edb.json"

STIX_CPE_PATH = "/home/giacomo/CNR/Osservatorio_cyber/Threat_identification/CPE_DB/stix-cpe-database-with-cve.json"
ATTACK_PATTERN_PATH = "/home/giacomo/CNR/Osservatorio_cyber/Threat_identification/CPE_DB/cti-master/capec/attack-pattern/*.json"
CAPEC_RELATIONSHIP_PATH = "/home/giacomo/CNR/Osservatorio_cyber/Threat_identification/CPE_DB/cti-master/capec/relationship/*.json"
COURSE_OF_ACTION_PATH = "/home/giacomo/CNR/Osservatorio_cyber/Threat_identification/CPE_DB/cti-master/capec/course-of-action/*.json"